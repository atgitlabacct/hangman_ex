defmodule Dictionary.WordList do
  @name __MODULE__

  def start_link() do
    Agent.start_link(&word_list/0, name: @name)
  end

  def random_word() do
    Agent.get(@name, fn words ->
      words
      |> Enum.random()
    end)
  end

  def word_list(file_path \\ "../../assets/words.txt") do
    file_path
    |> Path.expand(__DIR__)
    |> File.read!()
    |> String.split(~r/\n/)
  end
end
