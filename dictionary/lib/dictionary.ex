defmodule Dictionary do
  alias Dictionary.WordList

  @typedoc "Return values of `start*` functions"
  @type on_start :: {:ok, pid} | {:error, {:already_started, pid} | term}

  @spec start :: on_start
  defdelegate start(), to: WordList, as: :start_link

  @doc """
  Generates a random_word
  """
  @spec random_word() :: String.t()
  defdelegate random_word(), to: WordList
end
