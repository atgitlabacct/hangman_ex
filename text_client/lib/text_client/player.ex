defmodule TextClient.Player do
  alias TextClient.{State, Summary, Prompter, Mover}

  def play(%State{
        tally: %Hangman.Tally{game_state: :won, letters: letters},
        game_service: game_service
      }) do
    IO.puts("You WON! #{letters}")
    Hangman.end_game(game_service)
  end

  def play(%State{
        game_service: game_service,
        tally: %Hangman.Tally{game_state: :lost, letters: letters}
      }) do
    IO.puts("Sorry, You lost! :(")
    Hangman.end_game(game_service)
  end

  def play(game = %State{tally: %{game_state: :good_guess}}) do
    continue_with_message(game, "Good guess!")
  end

  def play(game = %State{tally: %{game_state: :bad_guess}}) do
    continue_with_message(game, "Bad guess!")
  end

  def play(game = %State{tally: %{game_state: :already_used}}) do
    continue(game)
  end

  def play(game = %State{tally: %{game_state: :initializing}}) do
    continue(game)
  end

  #############################################################################

  defp continue(game) do
    game
    |> Summary.display()
    |> Prompter.accept_move()
    |> Mover.make_move()
    |> play()
  end

  defp continue_with_message(game, msg) do
    IO.puts(msg)
    continue(game)
  end
end
