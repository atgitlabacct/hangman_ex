defmodule TextClient.Interact do
  alias TextClient.{State, Player}

  def start() do
    Hangman.new_game(:localhost)
    |> State.setup_state()
    |> Player.play()
  end

  def stop() do
    Hangman.end_game(:localhost)
  end

  ##############################################################################
end
