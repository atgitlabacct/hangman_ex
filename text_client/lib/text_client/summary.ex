defmodule TextClient.Summary do
  def display(game = %{tally: tally, game_service: game_service}) do
    IO.puts([
      "\n",
      "letters used: #{Enum.join(tally.used, ", ")}",
      "\n",
      "word so far: #{Enum.join(tally.letters, " ")}",
      "\n",
      "Guesses left: #{tally.turns_left}"
    ])

    game
  end
end
