defmodule TextClient.Prompter do
  @moduledoc """
  Receives and validates the input data from stdin.
  """

  alias TextClient.State

  def accept_move(game = %State{}) do
    IO.gets("Your guess: ")
    |> check_input(game)
  end

  #############################################################################
  defp check_input({:error, reason}, _) do
    IO.puts("Game ended: #{reason}")
    exit(:normal)
  end

  defp check_input(:eof, _) do
    IO.puts("Looks like you game up...")
    exit(:normal)
  end

  defp check_input(input, game) do
    input = String.trim(input)

    cond do
      input =~ ~r/\A[a-z]\z/ ->
        %{game | guess: input}

      true ->
        IO.puts("Please enter a lowercase letter")
        accept_move(game)
    end
  end
end
