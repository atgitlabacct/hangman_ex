defmodule TextClient.State do
  @moduledoc """
  Hands the state of the Text Client.  The game_service contains the
  Hangman.Game structure of a structure like it.
  """
  alias TextClient.State

  @doc """
  Structure to hold the TextClient.State structure.  The
  tally key is used to hold the data coming back from
  Hangman.Tally.
  """
  defstruct(
    game_service: nil,
    tally: nil,
    guess: ""
  )

  @spec setup_state(pid) :: %State{}
  def setup_state(game_pid) when is_pid(game_pid) do
    %State{
      game_service: game_pid,
      tally: Hangman.tally(game_pid)
    }
  end
end
