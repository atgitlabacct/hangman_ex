defmodule TextClient do
  defdelegate start(), to: TextClient.Interact
  defdelegate stop(), to: TextClient.Interact
end
