defmodule HangmanTest do
  use ExUnit.Case
  doctest Hangman

  alias Hangman.Game

  test "state isn't changed for :won or :lost game" do
    for state <- [:won, :lost] do
      game = Game.new_game()
             |> Map.put(:game_state, state)

      {new_game, _tally} = Game.make_move(game, "a")
      assert ^game = new_game
    end
  end

  test "wins a game" do
    game = Game.new_game("cat")
    {game, _tally} = Game.make_move(game, "c")
    assert game.game_state == :good_guess

    {game, _tally} = Game.make_move(game, "c")
    assert game.game_state == :already_used

    {game, _tally} = Game.make_move(game, "a")
    assert game.game_state == :good_guess
    {game, _tally} = Game.make_move(game, "t")

    assert game.game_state == :won
  end

  test "loses a game" do
    game = %{ Game.new_game("cat") | turns_left: 2}

    {game, _tally} = Game.make_move(game, "z")
    assert game.turns_left == 1
    assert game.game_state == :bad_guess

    {game, _tally} = Game.make_move(game, "z")
    assert game.game_state == :already_used
    assert game.turns_left == 1

    {game, _tally} = Game.make_move(game, "n")
    assert game.turns_left == 0
    assert game.game_state == :lost
  end

  test "non lowercase returns an error and unchanged game" do
    game = Game.new_game("cat")
    assert {:error, "Invalid move"} ==  Game.make_move(game, "Z")
  end

end
