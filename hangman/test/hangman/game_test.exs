defmodule Hangman.GameTest do
  use ExUnit.Case
  doctest Hangman.Game

  alias Hangman.Game

  describe "making a move" do
    test "returns :lost when turns_left: 0" do
      game = Game.new_game("cat")
      {game, _tally} = Game.make_move(game, "q")
      assert game.game_state == :bad_guess
      {game, _tally} = Game.make_move(game, "r")
      assert game.game_state == :bad_guess
      {game, _tally} = Game.make_move(game, "s")
      assert game.game_state == :bad_guess
      {game, _tally} = Game.make_move(game, "g")
      assert game.game_state == :bad_guess
      {game, _tally} = Game.make_move(game, "h")
      assert game.game_state == :bad_guess
      {game, _tally} = Game.make_move(game, "z")
      assert game.game_state == :bad_guess
      {game, _tally} = Game.make_move(game, "x")
      assert game.game_state == :lost
    end

    test "returns :won when game is won" do
      game = Game.new_game("cat")
      {game, _tally} = Game.make_move(game, "c")
      assert game.game_state == :good_guess
      {game, _tally} = Game.make_move(game, "a")
      assert game.game_state == :good_guess
      {game, _tally} = Game.make_move(game, "t")
      assert game.game_state == :won
    end
  end
end
