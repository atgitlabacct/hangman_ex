defmodule Hangman.Game do
  alias Hangman.{Game, Tally}

  defstruct(
    turns_left: 7,
    game_state: :initializing,
    letters: [],
    used: MapSet.new()
  )

  @type game :: %Game{}
  @type tally :: %Tally{}
  @type game_and_tally :: {game, tally}
  @type error :: {:error, String.t()}

  @valid_guesses ~w[a b c d e f g h i j k l m n o p q r s t u v w x y z]

  @spec new_game(none | String.t()) :: game
  def new_game do
    new_game(Dictionary.random_word())
  end

  def new_game(word) when is_binary(word) do
    %Game{
      letters: word |> String.codepoints()
    }
  end

  @doc """
  Makes a move, returns the tally (when won/lost) or returns an error
  """
  @spec make_move(game, String.t()) :: game_and_tally | error
  def make_move(game = %Game{game_state: state}, _guess) when state in [:won, :lost] do
    return_with_tally(game)
  end

  def make_move(game = %Game{}, guess) when guess in @valid_guesses do
    accept_move(game, guess, MapSet.member?(game.used, guess))
    |> return_with_tally
  end

  def make_move(game = %Game{}, _guess), do: {:error, "Invalid move"}

  @spec tally(game) :: tally
  def tally(game = %Game{}) do
    %Hangman.Tally{
      game_state: game.game_state,
      turns_left: game.turns_left,
      used: game.used |> MapSet.to_list() |> Enum.sort(),
      letters: game.letters |> reveal_guessed(game.used)
    }
  end

  ####################################################################
  # Privates
  ####################################################################

  defp accept_move(game, _guess, _already_used = true) do
    IO.puts("Guess already used")
    Map.put(game, :game_state, :already_used)
  end

  defp accept_move(game, guess, _already_used = false) do
    Map.put(game, :used, MapSet.put(game.used, guess))
    |> score_guess(Enum.member?(game.letters, guess))
  end

  defp score_guess(game, _good_guess = true) do
    new_state =
      MapSet.new(game.letters)
      |> MapSet.subset?(game.used)
      |> maybe_won

    %{game | game_state: new_state}
  end

  defp score_guess(game = %Game{turns_left: 1}, _bad_guess = false) do
    %{game | turns_left: 0, game_state: :lost}
  end

  defp score_guess(game = %Game{turns_left: turns}, _bad_guess = false) do
    %{game | turns_left: turns - 1, game_state: :bad_guess}
  end

  defp maybe_won(true), do: :won
  defp maybe_won(false), do: :good_guess

  defp reveal_guessed(letters, used) do
    letters
    |> Enum.map(fn letter -> reveal_letter(letter, MapSet.member?(used, letter)) end)
  end

  defp reveal_letter(letter, _in_word = true), do: letter
  defp reveal_letter(_letter, _in_word = false), do: "_"

  defp return_with_tally(game), do: {game, tally(game)}
end
