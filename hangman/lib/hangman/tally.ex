defmodule Hangman.Tally do
  defstruct(
    game_state: :initializing,
    turns_left: 7,
    used: [],
    letters: []
  )
end
