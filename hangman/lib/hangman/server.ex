defmodule Hangman.Server do
  use GenServer

  alias Hangman.Game

  ############################################################
  ## External Api
  def start_link(game_name, word \\ nil) do
    GenServer.start_link(__MODULE__, {word, game_name}, name: game_name)
  end

  def make_move(pid, guess) do
    GenServer.call(pid, {:make_move, guess})
  end

  def tally(pid) do
    GenServer.call(pid, :tally)
  end

  def stop(pid) do
    GenServer.stop(pid, :normal)
  end

  ############################################################
  ## Callbacks
  def init({word, game_name}) do
    IO.puts("New game started - #{game_name}")
    {:ok, Game.new_game()}
  end

  def init(word) when is_binary(word) do
    {:ok, Game.new_game(word)}
  end

  def handle_call({:make_move, guess}, _from, game) do
    case Game.make_move(game, guess) do
      error = {:error, msg} ->
        {:reply, error, game}

      {game, tally} ->
        {:reply, tally, game}
    end
  end

  def handle_call(:tally, _from, game) do
    tally = Game.tally(game)
    {:reply, tally, game}
  end

  ############################################################
  ## Interal Api
end
