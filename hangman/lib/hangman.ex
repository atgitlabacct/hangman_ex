defmodule Hangman do
  alias Hangman.Server

  @typedoc "Pid or atom name of process"
  @type name_or_pid :: pid | atom

  @typedoc "Tally of current game"
  @type tally :: %Hangman.Tally{}

  @doc """
  Creates a new game based on os_time (possible race condition?)
  """
  @spec new_game :: atom
  def new_game() do
    System.os_time()
    |> to_string
    |> String.to_atom()
    |> new_game()
  end

  @doc """
  Creates a new game with the name given
  """
  @spec new_game(atom) :: atom
  def new_game(game_name) when is_atom(game_name) do
    {:ok, pid} = Supervisor.start_child(Hangman.Supervisor, [game_name])
    game_name
  end

  @doc """
  Creates a new game with given name and word
  """
  @spec new_game(atom, String.t()) :: pid
  def new_game(game_name, word)
      when is_atom(game_name) and is_binary(word) do
    if nil = Process.whereis(game_name) do
      {:error, "Game already registered"}
    else
      {:ok, pid} = Supervisor.start_child(Hangman.Supervisor, [game_name, word])
      pid
    end
  end

  @doc """
  Stops the game with the given pid/name
  """
  @spec end_game(name_or_pid) :: :ok
  defdelegate end_game(game_id), to: Server, as: :stop

  @doc """
  Returns the %Tally{} of a game
  """
  @spec tally(name_or_pid) :: tally
  defdelegate tally(game_pid), to: Server, as: :tally

  @doc """
  Makes a moves
  """
  @spec make_move(name_or_pid, String.t()) :: tally
  defdelegate make_move(game_pid, guess), to: Server, as: :make_move
end
