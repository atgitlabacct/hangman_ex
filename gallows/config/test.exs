use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :gallows, GallowsWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

defmodule Gallows.GameClient.Fake do
  def new_game do
    TestGame
  end

  def tally(game) do
    %Hangman.Tally{}
  end

  def make_move(game, :error), do: {:error, "Test Error"}
  def make_move(game, :won), do: %Hangman.Tally{game_state: :won}
  def make_move(game, :lost), do: 
  def make_move(game, guess), do: 

  ####################################################################
  # Testing out how to use a stub from a controller
  #
  # Next step would be to finish the stubbing.
  ####################################################################
  defp fake_tally(:won) do
    %Hangman.Tally{game_state: :won, turns_left: 7, letters: ~w(c a t), used: ~w(c a t)}
  end

  defp fake_tally(:lost) do
    %Hangman.Tally{game_state: :lost, turns_left: 0, letters: ~w(c a t), used: ~w(z q w s b g k)}
  end

  defp fake_tally(:bad_guess) do
    %Hangman.Tally{game_state: :lost, turns_left: 6, letters: ~w(c a), used: ~w(c a z)}
  end

  defp fake_tally(:good_guess) do
    %Hangman.Tally{game_state: :good_guess, turns_left: 7, letters: ~w(c a), used: ~w(c a)}
  end
end
