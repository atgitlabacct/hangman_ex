defmodule GallowsWeb.HangmanView do
  use GallowsWeb, :view

  def word(tally) do
    tally.letters |> Enum.join(" ")
  end
end
