defmodule GallowsWeb.Api.HangmanController do
  use GallowsWeb, :controller
  @game_client Application.get_env(:gallows, :game_client)

  # POST
  # Create a new game
  def create_game(conn, _params) do
    game = Hangman.new_game()
    tally = Hangman.tally(game)

    json(conn, encode_to_json(%{game: game, tally: tally}))
  end

  PUT
  # Make a guess for a game
  def make_guess(conn, params = %{"guess" => %{"guess" => guess}}, opts \\ []) do
    game = get_session(conn, :game)

    case Hangman.make_move(game, guess) do
      {:error, msg} ->
        tally = Hangman.tally(game)
        json(conn, encode_to_json(%{error: msg, tally: tally}))

      tally ->
        json(conn, encode_to_json(%{tally: tally}))
    end
  end

  defp encode_to_json(val), do: Poison.encode!(val)
end
