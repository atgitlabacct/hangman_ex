defmodule GallowsWeb.HangmanController do
  use GallowsWeb, :controller

  def new_game(conn, _params) do
    render(conn, "new_game.html")
  end

  # POST
  # Create a new game
  def create_game(conn, _params) do
    game = Hangman.new_game()
    tally = Hangman.tally(game)

    put_session(conn, :game, game)
    |> render("game_field.html", tally: tally)
  end

  PUT
  # Make a guess for a game
  def make_guess(conn, params = %{"guess" => %{"guess" => guess}}) do
    game = get_session(conn, :game)

    case Hangman.make_move(game, guess) do
      {:error, msg} ->
        tally = Hangman.tally(game)

        conn
        |> put_flash(:error, msg)
        |> render("game_field.html", tally: tally)

      tally ->
        conn
        |> render("game_field.html", tally: tally)
    end
  end
end
